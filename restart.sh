#!/bin/bash
wget -q --tries=2 --timeout=10 --spider http://google.com
if [[ $? != 0 ]]; then
	csrftokenlogin="$(curl http://192.168.100.1/ -s | grep CSRFValue | sed 's/[^0-9]*//g')"
	curl --data "CSRFValue=${csrftokenlogin}&loginUsername=admin&loginPassword=elquesea" http://192.168.100.1/goform/login -s
	csrftokenreset="$(curl http://192.168.100.1/RgSecurity.asp -s | grep CSRFValue | sed 's/[^0-9]*//g')"
	curl -v --data "CSRFValue=${csrftokenreset}&mCmReset=1" http://192.168.100.1/goform/RgSecurity
	echo "Modem is reset"
fi